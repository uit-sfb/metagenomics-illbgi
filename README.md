# metagenomics-IllBgi

Analysis pipeline for Illumina and BGI sequenced Metagenomics dataset using Nextflow

****

**Usage:**

`nextflow run metagenomics.nf --illumina <path to the directory containing the illumina read file> --bgi <path to the directory containing the BGI read file>  -with-docker`

>Note: Path is the directory containing the fastq file with extension "fastq.gz"

Help information:
---
    nextflow run metagenomics.nf --help
        Input:
        --bgi                      path to the directory containing the BGI read file (fastq) (default: $params.bgi)
        --illumina                 path to the directory containing the illumina read file (fastq) (default: $params.illumina)
        
        Output: (path is fixed; prints result in default directory)
        --outputbgi                path to the output directory (default: $params.outputbgi)
        --outputill                path to the output directory (default: $params.outputill)
        
        Taxonomic database: (path is fixed now)
        --kaiju_refseq             Database (Refseq bactarch) for taxonomic binning with kaiju (default: $params.refseqkaiju). E.g. "/storage/aku048/database/refseq/kaiju1.7.3/refseq_arch_bact_default/refseq/kaiju_db_refseq.fmi"
        --kaiju_refseq_nodes       Database (Refseq bactarch) for taxonomic nodes with kaiju bactarch (default: $params.refseqkaiju). E.g. "/storage/aku048/database/refseq/kaiju1.7.3/refseq_arch_bact_default/nodes.dmp"
        --kraken_refseq            Database (Refseq bactarch) for taxonomic binning with kraken2 (default: $params.refseqkraken). E.g. "/storage/aku048/database/refseq/kraken2/"
        
        --kaiju_marRef             Database (marRef prot) for taxonomic binning with kaiju (default: $params.mar_refprot). E.g. "/storage/aku048/database/mar5/kaiju_mar_ref_protein/marref_proteins_V5.fmi"
        --kraken_marRef            Database (marRef nucl) for taxonomic binning with kraken2 (default: $params.mar_refnucl). E.g. "/storage/aku048/database/mar5/kraken_mar_ref_nucl/database/"
        --kaiju_marDb              Database (marDb prot) for taxonomic binning with kaiju (default: $params.mar_dbprot). E.g. "/storage/aku048/database/mar5/kaiju_mar_db_protein/masked_segmar5_db_prot.fmi"
        --kraken_marDb             Database (marDb nucl) for taxonomic binning with kraken2 (default: $params.mar_dbnucl). E.g. "/storage/aku048/database/mar5/kraken_mar_db_nucl/database/"
        
        --kaiju_marRefDb           Database (marRefDb combined prot) for taxonomic binning with kaiju (default: $params.mar_ref_dbprot). E.g. "/storage/aku048/database/mar5/kaiju_mar_ref_db_protein/mar5_ref_db_prot.fmi"
        --kraken_marRefDb          Database (marRefDb combined nucl) for taxonomic binning with kraken2 (default: $params.mar_ref_dbnucl). E.g. "/storage/aku048/database/mar5/kraken_mar_ref_db_nucl/database/"

        Miscellaneous:             
        -with-docker              Enable process execution in a Docker container
        -resume                   Execute the script using the cached results, useful to continue executions that was stopped by an error
        -bg                       Execute nextflow in background
        -h                        Print Nextflow help
        --help                    Print this Pipeline help


****
**Installation:**

Build docker images

`docker build -t hunter .`

****
**Workflow:**
```mermaid
graph TB
  subgraph "Preprocessing Illumina"
  Node1[FastQC] --> Node2[Optical duplicates]
  Node2 --> Node3[bbduk.sh] --> Node4[Fastq_screen]
  Node4 --> Node5[repair.sh] --> Node6[FastQC/MultiQC]
  end
subgraph "Preprocessing BGI"
  Nodes1[FastQC] --> Nodes2[bbduk.sh] --> Nodes3[repair.sh] --> Nodes5[FastQC/MultiQC]
  end

subgraph "Taxonomy Profiling"
  Nodes3 --> Tax1[Refseq] 
  Node5 --> Tax1[Refseq]
  Nodes3 & Node5 --> Tax2[MarDB, MarRef, MarDbRef] 
  Tax1 & Tax2 --> kaiju & kraken2
  end

subgraph "Assembly"
  Nodes3 --> Assemb[Megahit] 
  Node5 --> Assemb[Megahit]
  Assemb -- assembly stat --> stat[Quast]
  end

subgraph "Binning"
  Assemb --> Bin1[Maxbin2] 
  Assemb --> Bin2[Metabat2]
  Bin1 & Bin2 --> Bin3[checkM]
  end
```
