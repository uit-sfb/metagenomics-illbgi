#!/usr/bin/env nextflow
nextflow.enable.dsl=2

/*
===================================================================================
                    Marine Metagenomics analysis Pipeline                     
===================================================================================

nextflow/metagenomics Analysis Pipeline. Started 2020-10-10
#### Homepage / Documentation
https://github.com/Animesh911/Metagenomics
#### Author
Animesh Kumar <animesh.kumar@uit.no>
-----------------------------------------------------------------------------------
*/

// Help Message
def helpMessage() {
    log.info """
    *********Marine metagenomics analysis*********
    
    Usage:
    nextflow run metagenomics.nf -with-docker --illumina /storage/aku048/nextflow/marine/raw_data/ --bgi /storage/aku048/nextflow/marine/bgi/raw_data/ --outputill /storage/aku048/nextflow/marine/illumina/ --outputbgi /storage/aku048/nextflow/marine/bgi/ -resume
    
    Input:
    --bgi                      path to the directory containing the BGI read file (fastq) (default: $params.bgi)
    --illumina                 path to the directory containing the illumina read file (fastq) (default: $params.illumina)
    
    Output:
    --outputbgi                path to the output directory (default: $params.outputbgi)
    --outputill                path to the output directory (default: $params.outputill)
    
    Taxonomic database:
    --kaiju_refseq             Database (Refseq bactarch) for taxonomic binning with kaiju (default: $params.refseqkaiju). E.g. "/storage/aku048/database/refseq/kaiju1.7.3/refseq_arch_bact_default/refseq/kaiju_db_refseq.fmi"
    --kaiju_refseq_nodes       Database (Refseq bactarch) for taxonomic nodes with kaiju bactarch (default: $params.refseqkaiju). E.g. "/storage/aku048/database/refseq/kaiju1.7.3/refseq_arch_bact_default/nodes.dmp"
    --kraken_refseq            Database (Refseq bactarch) for taxonomic binning with kraken2 (default: $params.refseqkraken). E.g. "/storage/aku048/database/refseq/kraken2/"
    
    --kaiju_marRef             Database (marRef prot) for taxonomic binning with kaiju (default: $params.mar_refprot). E.g. "/storage/aku048/database/mar5/kaiju_mar_ref_protein/marref_proteins_V5.fmi"
    --kraken_marRef            Database (marRef nucl) for taxonomic binning with kraken2 (default: $params.mar_refnucl). E.g. "/storage/aku048/database/mar5/kraken_mar_ref_nucl/database/"
    --kaiju_marDb              Database (marDb prot) for taxonomic binning with kaiju (default: $params.mar_dbprot). E.g. "/storage/aku048/database/mar5/kaiju_mar_db_protein/masked_segmar5_db_prot.fmi"
    --kraken_marDb             Database (marDb nucl) for taxonomic binning with kraken2 (default: $params.mar_dbnucl). E.g. "/storage/aku048/database/mar5/kraken_mar_db_nucl/database/"
    
    --kaiju_marRefDb           Database (marRefDb combined prot) for taxonomic binning with kaiju (default: $params.mar_ref_dbprot). E.g. "/storage/aku048/database/mar5/kaiju_mar_ref_db_protein/mar5_ref_db_prot.fmi"
    --kraken_marRefDb          Database (marRefDb combined nucl) for taxonomic binning with kraken2 (default: $params.mar_ref_dbnucl). E.g. "/storage/aku048/database/mar5/kraken_mar_ref_db_nucl/database/"
    
    """
}

if (params.help) { exit 0, helpMessage() }

//params.reads = "$baseDir/raw_data/ST192_{fw,rv}.fastq.gz"
params.adapter_ill = "$baseDir/adapters/adapters.fa"
params.adapter_bgi = "$baseDir/adapters/BGI.fa"
params.multiqc = "$baseDir/multiqc"
params.fastqscreen = "$baseDir/fastq_screen.conf"

//Default Database
params.refseqkaiju = "/storage/aku048/database/refseq/kaiju1.7.3/refseq_arch_bact_default/refseq/kaiju_db_refseq.fmi"
params.refseqkaijunodes = "/storage/aku048/database/refseq/kaiju1.7.3/refseq_arch_bact_default/nodes.dmp"
params.refseqkraken = "/storage/aku048/database/refseq/kraken2/"
params.mar_refnucl = "/storage/aku048/database/mar5/kraken_mar_ref_nucl/database/"
params.mar_refprot = "/storage/aku048/database/mar5/kaiju_mar_ref_protein/marref_proteins_V5.fmi"
params.mar_dbnucl = "/storage/aku048/database/mar5/kraken_mar_db_nucl/database/"
params.mar_dbprot = "/storage/aku048/database/mar5/kaiju_mar_db_protein/masked_segmar5_db_prot.fmi"
params.mar_ref_dbnucl = "/storage/aku048/database/mar5/kraken_mar_ref_db_nucl/database/"
params.mar_ref_dbprot = "/storage/aku048/database/mar5/kaiju_mar_ref_db_protein/mar5_ref_db_prot.fmi"


println """\
         M A R I N E - METAGENOMICS P I P E L I N E
         ==========================================
         reads_illumina   : ${params.illumina}
         reads_bgi        : ${params.bgi}
         outdir_illumina  : ${params.outputill}
         outdir_bgi       : ${params.outputbgi}
         """
         .stripIndent()




// Include modules for each tool
include { First_Fastqc; Optical; Clumpify; Fastqscreen; Repair; Second_Fastqc; Multiqc;  } from './processes/preprocessing.nf'
include { First_Fastqcb; Clumpifyb; Repairb; Second_Fastqcb; Multiqcb; } from './processes/preprocessing_bgi.nf'
include {Kraken2; Kraken2 as Kraken_bgi; Kaiju; Kaiju as Kaiju_bgi} from './processes/taxonomy.nf'
include {Assembly; Assembly as Assembly_bgi} from './processes/assembly.nf'
include {Mapping; Mapping as Mapping_bgi; Maxbin; Metabat} from './processes/binning.nf'

//Preprocessing
workflow illumina {
  main:
    channel.fromFilePairs( "${params.illumina}/*_{fw,rv}.fastq.gz", checkIfExists: true ) | (First_Fastqc & Optical)  
    Clumpify(Optical.out.optical, params.adapter_ill)
    Fastqscreen(Clumpify.out.trim_r1, params.fastqscreen) | Repair | Second_Fastqc 
    Multiqc(First_Fastqc.out.dir, Second_Fastqc.out.dir)
  emit:
    data = Repair.out 
}

workflow bgi {
  main:

    input_bgi= channel.fromFilePairs( "${params.bgi}/*_{1,2}.fastq.gz", checkIfExists: true ) 
    First_Fastqcb(input_bgi)
    Clumpifyb(input_bgi, params.adapter_bgi) 
    Repairb(Clumpifyb.out.trim) | Second_Fastqcb 
    Multiqcb(First_Fastqcb.out.dir, Second_Fastqcb.out.dir)
  emit:
    data = Repairb.out  
}

//Taxonomic classification
workflow taxonomy_ill{
    take: my_data
    main:
      Kraken2(params.refseqkraken, params.mar_ref_dbnucl, my_data)
      Kaiju(params.refseqkaijunodes, params.refseqkaiju, params.mar_ref_dbprot, my_data)
}

workflow taxonomy_bgi{
    take: data
    main:
      Kraken_bgi(params.refseqkraken, params.mar_ref_dbnucl, data)
      Kaiju_bgi(params.refseqkaijunodes, params.refseqkaiju, params.mar_ref_dbprot, data)
}


//Assembly
workflow assembly_ill{
    take: my_data
    main:
      Assembly(my_data)
    emit:
      contigs = Assembly.out
}

workflow assembly_bgi{
    take: data
    main:
      Assembly_bgi(data)
    emit:
      contig = Assembly_bgi.out
}


//Binning
workflow binning_ill{
    take: contigs
          data
    main:
      Mapping(contigs, data)
      Maxbin(contigs, Mapping.out.abun, data)
      Metabat(contigs, Mapping.out.sort, data)
}

workflow binning_bgi{
    take: my_data
          data1
    main:
      Mapping_bgi(my_data, data1)
}


//Final Call
workflow {
    
    main:
      illumina()
      bgi()
      
      taxonomy_ill(illumina.out.data)  
      assembly_ill(illumina.out.data)
      binning_ill(assembly_ill.out.contigs, illumina.out.data)
      
      
      assembly_bgi(bgi.out.data)
      taxonomy_bgi(bgi.out.data)
//      binning_bgi(assembly_bgi.out.contig, bgi.out.data)
}


workflow.onComplete {
	log.info ( workflow.success ? "\nAnalysis Done!\n" : "Oops ... something went wrong" )}
 

//Done
