process First_Fastqcb {
  echo true
  tag "$sample_id"
  publishDir "$baseDir/bgi/preprocessing_bgi/${sample_id}/", mode: 'copy'

  input:
  tuple val(sample_id), path(reads)

  output:
  tuple val(sample_id), path ('pre_screen'), emit: dir

  shell:
  '''
  mkdir pre_screen
  fastqc -o pre_screen !{reads}
  '''
}

process Second_Fastqcb {

  echo true
  tag "$sample_id"
  publishDir "$baseDir/bgi/preprocessing_bgi/${sample_id}/", mode: 'copy'

  input:
  tuple val(sample_id), path (r1)

  output:
  tuple val(sample_id), path ('post_screen'), emit: dir

  shell:
  '''
  mkdir post_screen
  fastqc -o post_screen !{r1}
  '''
}

process Multiqcb {

  tag "$sample_id"
  publishDir "$baseDir/bgi/preprocessing_bgi/${sample_id}/", mode: 'copy'

  input:
  tuple val(sample_id), path (before)
  tuple val(sample_id), path (after)

  output:
  tuple val(sample_id), path ("multiqc"), emit: dir

  shell:
  '''
  multiqc --outdir multiqc !{before} !{after}
  '''
}

process Clumpifyb {
  
  publishDir "$baseDir/bgi/preprocessing_bgi/${sample_id}/clumpify"
  tag "$sample_id"
  echo true

  input:
  tuple val(sample_id), path (reads)
  path r3
  
  output:
  tuple val(sample_id), path ("*.fq.gz"), emit: trim
  
  shell:
  """
  bbduk.sh in1=${reads[0]} in2=${reads[1]} out1=${reads[0].getSimpleName()}.fq.gz out2=${reads[1].getSimpleName()}.fq.gz ref=!{r3} stats=Stats_${reads[0].getSimpleName()}.txt minlen=51 trimq=15 forcetrimleft=3
  """
}

process Repairb {

  tag "$sample_id"
  publishDir "$baseDir/bgi/preprocessing_bgi/${sample_id}/repair"
  container 'hunter:latest'

  input:
  tuple val(sample_id), path (r1)

  output:
  tuple val(sample_id), path ("*_fixed.fq.gz"), emit: synced_r1

  shell:
  """
  repair.sh in=!{r1[0]} in2=!{r1[1]} out=!{r1[0].getSimpleName()}_fixed.fq.gz out2=!{r1[1].getSimpleName()}_fixed.fq.gz outs=!{r1[0].getSimpleName()}_singletons.fq.gz repair
  """
}
