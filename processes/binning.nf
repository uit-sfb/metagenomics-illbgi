process Mapping {
  publishDir "$baseDir/binning/${sample_id}/"
  container 'hunter:latest'

  input:
  path contig
  tuple val(sample_id), path (reads)

  output:
  path "${reads[0].getSimpleName()}.sam", emit: bin
  path "${reads[0].getSimpleName()}.sorted.bam", emit: sort
  path "${reads[0].getSimpleName()}.cov.txt", emit: cov 
  path "${reads[0].getSimpleName()}.abun.txt", emit: abun

  shell:
  '''
  mkdir binning
  bbmap.sh ref=!{contig} in=!{reads[0]} in2=!{reads[1]} out=!{reads[0].getSimpleName()}.sam threads=24 nodisk 
  samtools faidx !{contig}
  samtools view -bShu !{reads[0].getSimpleName()}.sam | samtools sort -m 61G -@ 3 - -o !{reads[0].getSimpleName()}.sorted.bam
  pileup.sh in=!{reads[0].getSimpleName()}.sorted.bam out=!{reads[0].getSimpleName()}.cov.txt
  awk '{print $1"\\t"$5}' !{reads[0].getSimpleName()}.cov.txt | grep -v '^#' > !{reads[0].getSimpleName()}.abun.txt
  run_MaxBin.pl -contig !{contig} -out maxbin2 -abund !{reads[0].getSimpleName()}.abun.txt -min_contig_length 1000 -thread 12
  '''
}

process Maxbin {
  publishDir "$baseDir/binning/${sample_id}/", mode: 'copy', pattern:  "maxbin_bin"
  publishDir "$baseDir/binning/${sample_id}/", mode: 'copy', pattern:  "maxbin_checkm"
  container 'hunter:latest'

  input:
  path (contig)
  path (abun)
  tuple val(sample_id), path (reads)

  output:
  path "maxbin_bin", emit: maxbinout
  path "maxbin_checkm", emit: maxcheckm

  shell:
    """
    run_MaxBin.pl -contig ${contig} -out maxbin2 -abund !{abun.getSimpleName()}.abun.txt -min_contig_length 1000 -thread 12
    mkdir maxbin_bin
    mv maxbin2.*.fasta maxbin_bin/
    checkm lineage_wf -x fasta maxbin_bin/ maxbin_checkm/
    """
}


process Metabat {
  publishDir "$baseDir/binning/${sample_id}/", mode: 'copy', pattern: "metabat_bin"
  publishDir "$baseDir/binning/${sample_id}/", mode: 'copy', pattern:  "metabat_checkm"
  container 'hunter:latest'

  input:
  path (contig)
  path (sort)
  tuple val(sample_id), path (reads)

  output:
  tuple val(sample_id), path ("metabat_bin"), emit: metaout
  tuple val(sample_id), path ("metabat_checkm"), emit: metacheckm

  shell:
  '''
  mkdir !{sort.getSimpleName()}
  metabat2 -i !{contig} !{sort} -o metabat_bin/!{sort.getSimpleName()}
  checkm lineage_wf -x fa metabat_bin/ metabat_checkm/
    '''
}
