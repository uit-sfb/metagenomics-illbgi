process First_Fastqc {
  echo true
  publishDir "$baseDir/illumina/preprocessing_illum/${sample_id}/", mode: 'copy'

  input:
  tuple val(sample_id), path(reads)

  output:
  tuple val(sample_id), path ('pre_screen'), emit: dir

  shell:
  '''
  mkdir pre_screen
  fastqc -o pre_screen !{reads}
  '''
}

process Second_Fastqc {

  echo true
  publishDir "$baseDir/illumina/preprocessing_illum/${sample_id}/", mode: 'copy'

  input:
  tuple val(sample_id), path (r1)

  output:
  tuple val(sample_id), path ('post_screen'), emit: dir

  shell:
  '''
  mkdir post_screen
  fastqc -o post_screen !{r1}
  '''
}

process Multiqc {

  publishDir "$baseDir/illumina/preprocessing_illum/${sample_id}/", mode: 'copy'

  input:
  tuple val(sample_id), path (before)
  tuple val(sample_id), path (after)

  output:
  tuple val(sample_id), path ("multiqc"), emit: dir

  shell:
  '''
  multiqc --outdir multiqc !{before} !{after}
  '''
}

process Optical {
  
  publishDir "$baseDir/illumina/preprocessing_illum/${sample_id}/optical"
  tag "$sample_id"
  echo true

  input:
  tuple val(sample_id), path(reads)
  
  output:
  tuple val(sample_id), path ("*_clump.fastq.gz"), emit: optical
  
  shell:
  """
  clumpify.sh in1=${reads[0]} in2=${reads[1]} out1=${reads[0].getSimpleName()}_clump.fastq.gz out2=${reads[1].getSimpleName()}_clump.fastq.gz dedupe optical dist=40
  """
}

process Clumpify {
  
  publishDir "$baseDir/illumina/preprocessing_illum/${sample_id}/clumpify"
  tag "$sample_id"
  echo true

  input:
  tuple val(sample_id), path (reads)
  path r3
  
  output:
  tuple val(sample_id), path ("*_bbduk.fastq.gz"), emit: trim_r1
  
  shell:
  """
  bbduk.sh in1=${reads[0]} in2=${reads[1]} out1=${reads[0].getSimpleName()}_bbduk.fastq.gz  out2=${reads[1].getSimpleName()}_bbduk.fastq.gz ref=!{r3} forcetrimleft=17 ktrim=r minlen=51 qtrim=r trimq=10 tbo=t mink=11 hdist=1
  """
}

process Fastqscreen {

  container 'hunter:latest'
  publishDir "$baseDir/illumina/preprocessing_illum/${sample_id}/fastq_screen"

  input:
  tuple val(sample_id), path (r1)
  path r3

  output:
  tuple val(sample_id), path ("*.tagged_filter.fastq.gz"), emit: contamin_r1

  shell:
  """
  fastq_screen --nohits !{r1[0]} !{r1[1]} --conf !{r3} --aligner bowtie2
  """
}

process Repair {

  publishDir "$baseDir/illumina/preprocessing_illum/${sample_id}/repair"
  container 'hunter:latest'

  input:
  tuple val(sample_id), path (r1)

  output:
  tuple val(sample_id), path ("*.desensitized.fastq.gz"), emit: synced_r1

  shell:
  """
  repair.sh in=!{r1[0]} in2=!{r1[1]} out1=!{r1[0].getSimpleName()}.desensitized.fastq.gz  out2=!{r1[1].getSimpleName()}.desensitized.fastq.gz
  """
}

