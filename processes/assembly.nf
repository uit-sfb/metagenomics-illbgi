process Assembly {

  publishDir "$baseDir/assembly/${sample_id}/"
  container 'hunter:latest'

  input:
  tuple val(sample_id), path (reads)

  output:
  path "${reads[0].getSimpleName()}.contigs.fasta",  emit: contigs

  shell:
  """
  megahit -m 0.65 -t 38 -1 !{reads[0]} -2 !{reads[1]} --min-contig-len 200
  ln -s megahit_out/final.contigs.fa !{reads[0].getSimpleName()}.contigs.fasta
  """
}


process Metaquast {

  publishDir "$baseDir/assembly/${sample_id}/quast/"
  container 'hunter:latest'

  input:
  tuple val(sample_id), path contig

  output:
  path assembly,  emit: assembly_stat

  shell:
  """
  metaquast.py !{contig.getSimpleName()}.contigs.fasta
  """
}

process quast {
    tag "$assembler-$sample"
    publishDir "${params.outdir}/Assembly/$assembler", mode: params.publish_dir_mode

    input:
    set val(assembler), val(sample), file(assembly) from assembly_spades_to_quast.mix(assembly_megahit_to_quast).mix(assembly_spadeshybrid_to_quast)

    output:
    file("${sample}_QC/*") into quast_results

    when:
    !params.skip_quast

    script:
    """
    metaquast.py --threads "${task.cpus}" --rna-finding --max-ref-number 0 -l "${assembler}-${sample}" "${assembly}" -o "${sample}_QC"
    """
}