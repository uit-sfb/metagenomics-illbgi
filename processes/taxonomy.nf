process Kraken2 {
  publishDir "$baseDir/taxonomy/${sample_id}/"
  container 'hunter:latest'

  input:
  path refdb
  path marrefdb
  tuple val(sample_id), path (reads)

  output:
  path "${reads[0].getSimpleName()}.kraken_refseq_bact_arch.txt", emit: taxonomy_kraken_refdb
  path "${reads[0].getSimpleName()}.kraken_mar_ref_db_nucl.txt", emit: taxonomy_kraken_marrefdb

  shell:
  '''
  kraken2 --db !{refdb} --threads 12 --paired !{reads[0]} !{reads[1]} --gzip-compressed --output !{reads[0].getSimpleName()}.kraken_refseq_bact_arch.txt
  kraken2 --db !{marrefdb} --threads 12 --paired !{reads[0]} !{reads[1]} --gzip-compressed --output !{reads[0].getSimpleName()}.kraken_mar_ref_db_nucl.txt
    '''
}


process Kaiju {
  publishDir "$baseDir/taxonomy/${sample_id}/"
  container 'hunter:latest'

  input:
  path node
  path refdb
  path marrefdb
  tuple val(sample_id), path (reads)

  output:
  path "${reads[0].getSimpleName()}.kaiju_refseq_default_bact_arch.txt", emit: tax_kaiju_refdb
  path "${reads[0].getSimpleName()}.kaiju_masked_mar_ref_db_prot.txt", emit: tax_kaiju_marrefdb

  shell:
  '''
  kaiju -t !{node} -f !{refdb} -i !{reads[0]} -j !{reads[1]} -o !{reads[0].getSimpleName()}.kaiju_refseq_default_bact_arch.txt -z 12
  kaiju -t !{node} -f !{marrefdb} -i !{reads[0]} -j !{reads[1]} -o !{reads[0].getSimpleName()}.kaiju_masked_mar_ref_db_prot.txt -z 12
    '''
}






